package com.legacy.scuba_gear.item;

import java.util.function.Consumer;

import com.legacy.scuba_gear.ScubaRegistry;
import com.legacy.scuba_gear.client.ScubaBootsModel;
import com.legacy.scuba_gear.client.ScubaChestplateModel;
import com.legacy.scuba_gear.client.ScubaHelmetModel;
import com.legacy.scuba_gear.client.ScubaLayers;
import com.legacy.scuba_gear.client.ScubaLeggingsModel;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;
import net.minecraftforge.common.util.NonNullLazy;

public class ScubaGearItem extends ArmorItem
{
	public ScubaGearItem(ArmorMaterial materialIn, ArmorItem.Type slot, Properties builder)
	{
		super(materialIn, slot, builder);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void initializeClient(Consumer<IClientItemExtensions> consumer)
	{
		consumer.accept(Rendering.INSTANCE);
	}

	@OnlyIn(Dist.CLIENT)
	private static final class Rendering implements IClientItemExtensions
	{
		private static final Rendering INSTANCE = new ScubaGearItem.Rendering();

		private final NonNullLazy<ScubaHelmetModel<LivingEntity>> helmet = NonNullLazy.of(() -> new ScubaHelmetModel<LivingEntity>(getModel().bakeLayer(ScubaLayers.SCUBA_HELMET)));
		private final NonNullLazy<ScubaChestplateModel<LivingEntity>> chestplate = NonNullLazy.of(() -> new ScubaChestplateModel<LivingEntity>(getModel().bakeLayer(ScubaLayers.SCUBA_CHESTPLATE)));
		private final NonNullLazy<ScubaLeggingsModel<LivingEntity>> leggings = NonNullLazy.of(() -> new ScubaLeggingsModel<LivingEntity>(getModel().bakeLayer(ScubaLayers.SCUBA_LEGGINGS)));
		private final NonNullLazy<ScubaBootsModel<LivingEntity>> boots = NonNullLazy.of(() -> new ScubaBootsModel<LivingEntity>(getModel().bakeLayer(ScubaLayers.SCUBA_BOOTS)));

		private Rendering()
		{
		}

		@Override
		public net.minecraft.client.model.HumanoidModel<?> getHumanoidArmorModel(LivingEntity wearer, ItemStack stack, EquipmentSlot slot, net.minecraft.client.model.HumanoidModel<?> defaultModel)
		{
			Item item = stack.getItem();

			return item == ScubaRegistry.SCUBA_CHESTPLATE.get() ? this.chestplate.get() : item == ScubaRegistry.SCUBA_LEGGINGS.get() ? this.leggings.get() : item == ScubaRegistry.SCUBA_BOOTS.get() ? this.boots.get() : this.helmet.get();
		}

		@OnlyIn(Dist.CLIENT)
		private static net.minecraft.client.model.geom.EntityModelSet getModel()
		{
			return net.minecraft.client.Minecraft.getInstance().getEntityModels();
		}
	}
}
