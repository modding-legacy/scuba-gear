package com.legacy.scuba_gear;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

public class ScubaConfig
{
	public static final ForgeConfigSpec COMMON_SPEC;
	public static final CommonConfig COMMON;

	static
	{
		{
			Pair<CommonConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
			COMMON = pair.getLeft();
			COMMON_SPEC = pair.getRight();
		}
	}

	public static class CommonConfig
	{
		private final ConfigValue<Integer> distanceFromRuin;

		public CommonConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Server and Client side changes").push("common");
			distanceFromRuin = builder.comment("The max distance a Drowned can be from an Ocean Ruin to spawn with Scuba Gear. If Drowned cause performance issues, lower this.").define("distanceFromRuin", 15);
			
			builder.pop();
		}

		public int getMaxDistanceFromRuin()
		{
			return distanceFromRuin.get();
		}
	}
}