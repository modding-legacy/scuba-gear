package com.legacy.scuba_gear.client;

import com.google.common.collect.ImmutableList;
import com.legacy.scuba_gear.ScubaGearMod;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;

public class ScubaLeggingsModel<T extends LivingEntity> extends ScubaGearModel<T>
{
	private static final ResourceLocation TEXTURE = ScubaGearMod.locate("textures/models/armor/scuba_leggings.png");

	public ModelPart scubaPants;
	public ModelPart scubaRightLeg;
	public ModelPart scubaLeftLeg;

	public ScubaLeggingsModel(ModelPart model)
	{
		super(model);

		this.scubaRightLeg = model.getChild("right_pant_leg");
		this.scubaLeftLeg = model.getChild("left_pant_leg");
		this.scubaPants = model.getChild("pants");
	}

	public static LayerDefinition createLeggingsLayer(CubeDeformation size)
	{
		MeshDefinition mesh = HumanoidModel.createMesh(size, 0.0F);
		PartDefinition root = mesh.getRoot();

		root.addOrReplaceChild("left_pant_leg", CubeListBuilder.create().texOffs(24, 16).mirror().addBox(-1.75F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.4F)), PartPose.offset(2.0F, 12.0F, 0.0F));
		root.addOrReplaceChild("right_pant_leg", CubeListBuilder.create().texOffs(24, 16).addBox(-2.25F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.4F)), PartPose.offset(-2.0F, 12.0F, 0.0F));
		root.addOrReplaceChild("pants", CubeListBuilder.create().texOffs(0, 16).addBox(-4.0F, 0.0F, -2.0F, 8.0F, 12.0F, 4.0F, new CubeDeformation(0.3F)), PartPose.ZERO);

		return LayerDefinition.create(mesh, 64, 32);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		return ImmutableList.of();
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		this.scubaPants.copyFrom(this.body);
		this.scubaRightLeg.copyFrom(this.rightLeg);
		this.scubaLeftLeg.copyFrom(this.leftLeg);

		return ImmutableList.of(this.scubaPants, this.scubaRightLeg, this.scubaLeftLeg);
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);

		/*this.pants.copyModelAngles(this.bipedBody);
		this.rightLeg.copyModelAngles(this.bipedRightLeg);
		this.leftLeg.copyModelAngles(this.bipedLeftLeg);*/
	}

	@Override
	protected ResourceLocation getTexture()
	{
		return TEXTURE;
	}
}
