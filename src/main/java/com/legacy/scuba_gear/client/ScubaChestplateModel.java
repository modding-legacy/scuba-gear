package com.legacy.scuba_gear.client;

import com.google.common.collect.ImmutableList;
import com.legacy.scuba_gear.ScubaGearMod;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;

public class ScubaChestplateModel<T extends LivingEntity> extends ScubaGearModel<T>
{
	private static final ResourceLocation TEXTURE = ScubaGearMod.locate("textures/models/armor/scuba_chestplate.png");

	public ModelPart torso;
	public ModelPart rightArm;
	public ModelPart leftArm;

	public ScubaChestplateModel(ModelPart model)
	{
		super(model);

		this.rightArm = model.getChild("right_arm");
		this.leftArm = model.getChild("left_arm");
		this.torso = model.getChild("torso");
	}

	public static LayerDefinition createChestplateLayer(CubeDeformation size)
	{
		MeshDefinition mesh = HumanoidModel.createMesh(size, 0.0F);
		PartDefinition root = mesh.getRoot();

		CubeListBuilder rightArm = CubeListBuilder.create();
		rightArm.texOffs(0, 16).addBox(-3.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.5F));
		rightArm.texOffs(42, 16).addBox(-3.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.75F));
		root.addOrReplaceChild("right_arm", rightArm, PartPose.offset(-5.0F, 2.0F, 0.0F));

		CubeListBuilder leftArm = CubeListBuilder.create().mirror();
		leftArm.texOffs(0, 16).addBox(-1.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.5F));
		leftArm.texOffs(42, 16).addBox(-1.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.75F));
		root.addOrReplaceChild("left_arm", leftArm, PartPose.offset(2.0F, 12.0F, 0.0F));

		root.addOrReplaceChild("torso", CubeListBuilder.create().texOffs(16, 15).addBox(-4.0F, 0.0F, -2.5F, 8.0F, 12.0F, 5.0F, new CubeDeformation(0.4F)), PartPose.ZERO);

		return LayerDefinition.create(mesh, 64, 32);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		return ImmutableList.of();
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		/*hacky workaround since forge is broken and setRotationAngles isn't being called.*/
		this.rightArm.copyFrom(this.rightArm);
		this.leftArm.copyFrom(this.leftArm);
		this.torso.copyFrom(this.body);

		return ImmutableList.of(this.torso, this.rightArm, this.leftArm);
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);

		/*this.rightArm.copyModelAngles(this.bipedRightArm);
		this.leftArm.copyModelAngles(this.bipedLeftArm);
		this.torso.copyModelAngles(this.bipedBody);*/

		/*if (entityIn instanceof ZombieEntity)
		{
			boolean flag = ((ZombieEntity) entityIn).isAggressive();
			float f = MathHelper.sin(this.swingProgress * (float) Math.PI);
			float f1 = MathHelper.sin((1.0F - (1.0F - this.swingProgress) * (1.0F - this.swingProgress)) * (float) Math.PI);
			this.rightArm.rotateAngleZ = 0.0F;
			this.leftArm.rotateAngleZ = 0.0F;
			this.rightArm.rotateAngleY = -(0.1F - f * 0.6F);
			this.leftArm.rotateAngleY = 0.1F - f * 0.6F;
			float f2 = -(float) Math.PI / (flag ? 1.5F : 2.25F);
			this.rightArm.rotateAngleX = f2;
			this.leftArm.rotateAngleX = f2;
			this.rightArm.rotateAngleX += f * 1.2F - f1 * 0.4F;
			this.leftArm.rotateAngleX += f * 1.2F - f1 * 0.4F;
			this.rightArm.rotateAngleZ += MathHelper.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
			this.leftArm.rotateAngleZ -= MathHelper.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
			this.rightArm.rotateAngleX += MathHelper.sin(ageInTicks * 0.067F) * 0.05F;
			this.leftArm.rotateAngleX -= MathHelper.sin(ageInTicks * 0.067F) * 0.05F;
		}*/
	}

	@Override
	protected ResourceLocation getTexture()
	{
		return TEXTURE;
	}
}
