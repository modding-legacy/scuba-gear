package com.legacy.scuba_gear.client;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.AgeableListModel;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;
import net.minecraftforge.fml.util.ObfuscationReflectionHelper;

public abstract class ScubaGearModel<T extends LivingEntity> extends HumanoidModel<T>
{
	protected boolean isChildHeadScaled = ObfuscationReflectionHelper.getPrivateValue(AgeableListModel.class, this, "f_102007_");
	protected float childHeadScale = ObfuscationReflectionHelper.getPrivateValue(AgeableListModel.class, this, "f_102010_");
	protected float childHeadOffsetY = ObfuscationReflectionHelper.getPrivateValue(AgeableListModel.class, this, "f_170338_");
	protected float childHeadOffsetZ = ObfuscationReflectionHelper.getPrivateValue(AgeableListModel.class, this, "f_170339_");
	protected float childBodyScale = ObfuscationReflectionHelper.getPrivateValue(AgeableListModel.class, this, "f_102011_");
	protected float childBodyOffsetY = ObfuscationReflectionHelper.getPrivateValue(AgeableListModel.class, this, "f_102012_");

	public ScubaGearModel(ModelPart model)
	{
		super(model);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		return ImmutableList.of();
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return ImmutableList.of();
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
	}

	@Override
	public void renderToBuffer(PoseStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha)
	{
		VertexConsumer ivertexbuilder = Minecraft.getInstance().renderBuffers().bufferSource().getBuffer(RenderType.entityTranslucent(this.getTexture()));

		if (this.young)
		{
			matrixStackIn.pushPose();
			if (this.isChildHeadScaled)
			{
				float f = 1.5F / this.childHeadScale;
				matrixStackIn.scale(f, f, f);
			}

			matrixStackIn.translate(0.0D, (double) (this.childHeadOffsetY / 16.0F), (double) (this.childHeadOffsetZ / 16.0F));
			this.headParts().forEach((modelRenderer) ->
			{
				modelRenderer.render(matrixStackIn, ivertexbuilder, packedLightIn, packedOverlayIn, red, green, blue, alpha);
			});
			matrixStackIn.popPose();
			matrixStackIn.pushPose();
			float f1 = 1.0F / this.childBodyScale;
			matrixStackIn.scale(f1, f1, f1);
			matrixStackIn.translate(0.0D, (double) (this.childBodyOffsetY / 16.0F), 0.0D);
			this.bodyParts().forEach((modelRenderer) ->
			{
				modelRenderer.render(matrixStackIn, ivertexbuilder, packedLightIn, packedOverlayIn, red, green, blue, alpha);
			});
			matrixStackIn.popPose();
		}
		else
		{
			this.headParts().forEach((modelRenderer) ->
			{
				modelRenderer.render(matrixStackIn, ivertexbuilder, packedLightIn, packedOverlayIn, red, green, blue, alpha);
			});
			this.bodyParts().forEach((modelRenderer) ->
			{
				modelRenderer.render(matrixStackIn, ivertexbuilder, packedLightIn, packedOverlayIn, red, green, blue, alpha);
			});
		}
	}

	protected abstract ResourceLocation getTexture();

	public void setRotateAngle(ModelPart ModelRenderer, float x, float y, float z)
	{
		ModelRenderer.xRot = x;
		ModelRenderer.yRot = y;
		ModelRenderer.zRot = z;
	}
}