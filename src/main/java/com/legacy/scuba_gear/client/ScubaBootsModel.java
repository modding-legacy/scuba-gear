package com.legacy.scuba_gear.client;

import com.google.common.collect.ImmutableList;
import com.legacy.scuba_gear.ScubaGearMod;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;

public class ScubaBootsModel<T extends LivingEntity> extends ScubaGearModel<T>
{
	private static final ResourceLocation TEXTURE = ScubaGearMod.locate("textures/models/armor/scuba_boots.png");

	public ModelPart rightBoot;
	public ModelPart leftBoot;

	public ScubaBootsModel(ModelPart model)
	{
		super(model);
		
		this.rightBoot = model.getChild("right_boot");
		this.leftBoot = model.getChild("left_boot");
	}
	
	public static LayerDefinition createBootsLayer(CubeDeformation size)
	{
		MeshDefinition mesh = HumanoidModel.createMesh(size, 0.0F);
		PartDefinition root = mesh.getRoot();

		CubeListBuilder rightArm = CubeListBuilder.create();
		rightArm.texOffs(0, 16).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.75F));
		rightArm.texOffs(16, 18).addBox(-3.5F, 8.5F, -6.5F, 6.0F, 4.0F, 10.0F);
		root.addOrReplaceChild("right_boot", rightArm, PartPose.offset(-2.0F, 12.0F, 0.0F));

		CubeListBuilder leftArm = CubeListBuilder.create().mirror();
		leftArm.texOffs(0, 16).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.75F));
		leftArm.texOffs(16, 18).addBox(-2.5F, 8.5F, -6.5F, 6.0F, 4.0F, 10.0F);
		root.addOrReplaceChild("left_boot", leftArm, PartPose.offset(2.0F, 12.0F, 0.0F));

		return LayerDefinition.create(mesh, 64, 32);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		return ImmutableList.of();
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		this.rightBoot.copyFrom(this.rightLeg);
		this.leftBoot.copyFrom(this.leftLeg);

		return ImmutableList.of(this.rightBoot, this.leftBoot);
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);

		/*this.rightBoot.copyModelAngles(this.bipedRightLeg);
		this.leftBoot.copyModelAngles(this.bipedLeftLeg);*/
	}

	@Override
	protected ResourceLocation getTexture()
	{
		return TEXTURE;
	}
}
