package com.legacy.scuba_gear.client;

import com.legacy.scuba_gear.ScubaGearMod;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public class ScubaLayers
{
	public static final ModelLayerLocation SCUBA_HELMET = layer("scuba_helmet");
	public static final ModelLayerLocation SCUBA_CHESTPLATE = layer("scuba_chestplate");
	public static final ModelLayerLocation SCUBA_LEGGINGS = layer("scuba_leggings");
	public static final ModelLayerLocation SCUBA_BOOTS = layer("scuba_boots");

	public static void init()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(ScubaLayers::initLayers);
	}

	public static void initPost(FMLClientSetupEvent event)
	{
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(SCUBA_HELMET, () -> ScubaHelmetModel.createHelmetLayer(CubeDeformation.NONE));
		event.registerLayerDefinition(SCUBA_CHESTPLATE, () -> ScubaChestplateModel.createChestplateLayer(CubeDeformation.NONE));
		event.registerLayerDefinition(SCUBA_LEGGINGS, () -> ScubaLeggingsModel.createLeggingsLayer(CubeDeformation.NONE));
		event.registerLayerDefinition(SCUBA_BOOTS, () -> ScubaBootsModel.createBootsLayer(CubeDeformation.NONE));
	}

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(ScubaGearMod.locate(name), layer);
	}
}