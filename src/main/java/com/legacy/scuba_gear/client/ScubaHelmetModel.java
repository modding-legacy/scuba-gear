package com.legacy.scuba_gear.client;

import com.google.common.collect.ImmutableList;
import com.legacy.scuba_gear.ScubaGearMod;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;

public class ScubaHelmetModel<T extends LivingEntity> extends ScubaGearModel<T>
{
	private static final ResourceLocation TEXTURE = ScubaGearMod.locate("textures/models/armor/scuba_helmet.png");

	public ModelPart Helmet;
	public ModelPart helmetTorso;

	public ScubaHelmetModel(ModelPart model)
	{
		super(model);

		this.Helmet = model.getChild("helmet");
		this.helmetTorso = model.getChild("helmet_torso");
	}

	public static LayerDefinition createHelmetLayer(CubeDeformation size)
	{
		MeshDefinition mesh = HumanoidModel.createMesh(size, 0.0F);
		PartDefinition root = mesh.getRoot();

		// gotta scale it because the piglin heads are large
		CubeDeformation scale = new CubeDeformation(0.01F);
		CubeListBuilder helmet = CubeListBuilder.create();

		helmet.texOffs(0, 0).addBox(-5.0F, -9.0F, -5.0F, 10.0F, 10.0F, 10.0F, scale);
		helmet.texOffs(30, 0).addBox(-3.0F, -10.0F, -3.0F, 6.0F, 1.0F, 6.0F, scale);
		helmet.texOffs(46, 0).addBox(-6.0F, -8.0F, -4.0F, 1.0F, 8.0F, 8.0F, scale);
		helmet.texOffs(46, 16).addBox(5.0F, -8.0F, -4.0F, 1.0F, 8.0F, 8.0F, scale);
		root.addOrReplaceChild("helmet", helmet, PartPose.ZERO);

		root.addOrReplaceChild("helmet_torso", CubeListBuilder.create().texOffs(0, 20).addBox(-7.0F, -1.0F, -3.0F, 14.0F, 6.0F, 6.0F), PartPose.ZERO);

		return LayerDefinition.create(mesh, 64, 32);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		this.Helmet.copyFrom(this.head);
		this.helmetTorso.copyFrom(this.body);

		return ImmutableList.of(this.Helmet, this.helmetTorso);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return ImmutableList.of();
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		/*this.Helmet.copyModelAngles(this.bipedHead);
		this.helmetTorso.copyModelAngles(this.bipedBody);*/
	}

	@Override
	protected ResourceLocation getTexture()
	{
		return TEXTURE;
	}
}
