package com.legacy.scuba_gear;

import com.legacy.scuba_gear.client.ScubaLayers;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(ScubaGearMod.MODID)
public class ScubaGearMod
{
	public static final String NAME = "Scuba Gear";
	public static final String MODID = "scuba_gear";

	public ScubaGearMod()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ScubaConfig.COMMON_SPEC);

		MinecraftForge.EVENT_BUS.register(ScubaEvents.class);

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			FMLJavaModLoadingContext.get().getModEventBus().addListener(ScubaLayers::initPost);
			ScubaLayers.init();
		});

		FMLJavaModLoadingContext.get().getModEventBus().addListener(ScubaRegistry::registerCreativeTabs);
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(ScubaGearMod.MODID, name);
	}

	public static String find(String name)
	{
		return ScubaGearMod.MODID + ":" + name;
	}
}