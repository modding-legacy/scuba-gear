package com.legacy.scuba_gear;

import com.legacy.scuba_gear.item.ScubaGearItem;

import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterials;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

@EventBusSubscriber(modid = ScubaGearMod.MODID, bus = Bus.MOD)
public class ScubaRegistry
{
	public static final Lazy<Item> SCUBA_HELMET = Lazy.of(() -> new ScubaGearItem(ArmorMaterials.IRON, ArmorItem.Type.HELMET, new Item.Properties()));
	public static final Lazy<Item> SCUBA_CHESTPLATE = Lazy.of(() -> new ScubaGearItem(ArmorMaterials.IRON, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
	public static final Lazy<Item> SCUBA_LEGGINGS = Lazy.of(() -> new ScubaGearItem(ArmorMaterials.IRON, ArmorItem.Type.LEGGINGS, new Item.Properties()));
	public static final Lazy<Item> SCUBA_BOOTS = Lazy.of(() -> new ScubaGearItem(ArmorMaterials.IRON, ArmorItem.Type.BOOTS, new Item.Properties()));

	@SubscribeEvent
	public static void onRegister(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(ForgeRegistries.Keys.ITEMS))
		{
			event.register(ForgeRegistries.Keys.ITEMS, ScubaGearMod.locate("scuba_helmet"), SCUBA_HELMET);
			event.register(ForgeRegistries.Keys.ITEMS, ScubaGearMod.locate("scuba_chestplate"), SCUBA_CHESTPLATE);
			event.register(ForgeRegistries.Keys.ITEMS, ScubaGearMod.locate("scuba_leggings"), SCUBA_LEGGINGS);
			event.register(ForgeRegistries.Keys.ITEMS, ScubaGearMod.locate("scuba_boots"), SCUBA_BOOTS);
		}
	}
	
	public static void registerCreativeTabs(CreativeModeTabEvent.BuildContents event)
	{
		if (event.getTab() == CreativeModeTabs.COMBAT)
		{
			event.accept(SCUBA_HELMET.get());
			event.accept(SCUBA_CHESTPLATE.get());
			event.accept(SCUBA_LEGGINGS.get());
			event.accept(SCUBA_BOOTS.get());
		}
	}
}