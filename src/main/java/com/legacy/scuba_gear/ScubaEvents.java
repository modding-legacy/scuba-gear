package com.legacy.scuba_gear;

import java.util.List;
import java.util.stream.Collectors;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Drowned;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.levelgen.structure.BuiltinStructures;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ScubaEvents
{
	// just an easier way of calling all the pieces
	private static final List<Item> SCUBA_GEAR = List.of(ScubaRegistry.SCUBA_HELMET.get(), ScubaRegistry.SCUBA_CHESTPLATE.get(), ScubaRegistry.SCUBA_LEGGINGS.get(), ScubaRegistry.SCUBA_BOOTS.get());
	// lower the player's movement speed when using the gear above water
	private static final AttributeModifier HELMET_SPEED_MODIFIER = new AttributeModifier("scuba helmet land speed reduction", -0.003F, AttributeModifier.Operation.ADDITION);
	private static final AttributeModifier CHESTPLATE_SPEED_MODIFIER = new AttributeModifier("scuba chestplate land speed reduction", -0.005F, AttributeModifier.Operation.ADDITION);
	private static final AttributeModifier LEGGINGS_SPEED_MODIFIER = new AttributeModifier("scuba leggings land speed reduction", -0.002F, AttributeModifier.Operation.ADDITION);
	private static final AttributeModifier BOOTS_SPEED_MODIFIER = new AttributeModifier("scuba boots land speed reduction", -0.001F, AttributeModifier.Operation.ADDITION);
	// the player's swimming speed, used for the leggings (when swimming)
	private static final AttributeModifier SWIM_SPEED_MODIFIER = new AttributeModifier("scuba swim speed addition", 0.3F, AttributeModifier.Operation.ADDITION);
	// the player's underwater walking speed, used for the boots (when not swimming)
	private static final AttributeModifier UNDERWATER_WALK_SPEED_MODIFIER = new AttributeModifier("scuba underwater walk speed addition", 0.8F, AttributeModifier.Operation.ADDITION);

	/*@SubscribeEvent
	public static void onEntityJoinWorld(EntityJoinWorldEvent event)
	{
	}*/

	public static void onDrownedSpawn(Drowned drowned)
	{
		if (isRuinOrWreckNearby(drowned))
		{
			/*first the drowned will be able to spawn with a helmet, then boots, then leggings, and finally the chestplate*/
			float chance = drowned.level.random.nextFloat();

			if (chance < 0.50F)
				drowned.setItemSlot(EquipmentSlot.HEAD, new ItemStack(ScubaRegistry.SCUBA_HELMET.get()));
			if (chance < 0.40F)
				drowned.setItemSlot(EquipmentSlot.FEET, new ItemStack(ScubaRegistry.SCUBA_BOOTS.get()));
			if (chance < 0.30F)
				drowned.setItemSlot(EquipmentSlot.LEGS, new ItemStack(ScubaRegistry.SCUBA_LEGGINGS.get()));
			if (chance < 0.20F)
				drowned.setItemSlot(EquipmentSlot.CHEST, new ItemStack(ScubaRegistry.SCUBA_CHESTPLATE.get()));
		}
	}

	@SubscribeEvent
	public static void onLivingAttack(LivingAttackEvent event)
	{
		if (event.getSource().is(DamageTypes.LAVA) && isWearingFullScuba(event.getEntity()))
		{
			event.getEntity().setRemainingFireTicks(0);
			event.setCanceled(true);

			event.getEntity().getArmorSlots().forEach((armor) ->
			{
				if (event.getEntity() instanceof ServerPlayer && event.getEntity().tickCount % 10 == 0 && !event.getEntity().level.isClientSide)
					armor.hurtAndBreak(1, event.getEntity(), (living) ->
					{
					});
			});
		}
	}

	@SubscribeEvent
	public static void onLivingTick(LivingEvent.LivingTickEvent event)
	{
		LivingEntity living = event.getEntity();

		if (living instanceof Player || living instanceof Drowned)
		{
			// slow the player down when on land wearing the gear
			checkAddAndRemoveModifier(living, isWearingScubaOnSlot(living, EquipmentSlot.HEAD) && !living.isInWaterOrBubble(), Attributes.MOVEMENT_SPEED, HELMET_SPEED_MODIFIER);
			checkAddAndRemoveModifier(living, isWearingScubaOnSlot(living, EquipmentSlot.CHEST) && !living.isInWaterOrBubble(), Attributes.MOVEMENT_SPEED, CHESTPLATE_SPEED_MODIFIER);
			checkAddAndRemoveModifier(living, isWearingScubaOnSlot(living, EquipmentSlot.LEGS) && !living.isInWaterOrBubble(), Attributes.MOVEMENT_SPEED, LEGGINGS_SPEED_MODIFIER);
			checkAddAndRemoveModifier(living, isWearingScubaOnSlot(living, EquipmentSlot.FEET) && !living.isInWaterOrBubble(), Attributes.MOVEMENT_SPEED, BOOTS_SPEED_MODIFIER);

			// apply the swim speed boost when swimming, and wearing the leggings
			checkAddAndRemoveModifier(living, living.isVisuallySwimming() && isWearingScubaOnSlot(living, EquipmentSlot.LEGS), ForgeMod.SWIM_SPEED.get(), SWIM_SPEED_MODIFIER);

			// apply the walking speed boost when not swimming, and wearing the boots
			checkAddAndRemoveModifier(living, !living.isSwimming() && isWearingScubaOnSlot(living, EquipmentSlot.FEET), ForgeMod.SWIM_SPEED.get(), UNDERWATER_WALK_SPEED_MODIFIER);

			// give the player water breathing when wearing the helmet
			if (isWearingScubaOnSlot(living, EquipmentSlot.HEAD) && living.isEyeInFluidType(ForgeMod.WATER_TYPE.get()) && !living.level.isClientSide)
				living.addEffect(new MobEffectInstance(MobEffects.WATER_BREATHING, 05, 0, false, false, false));

			if (living instanceof Player && living.isInWaterOrBubble() && !living.level.isClientSide && living.tickCount % 80 == 0)
			{
				if (isWearingScubaOnSlot(living, EquipmentSlot.HEAD))
					living.getItemBySlot(EquipmentSlot.HEAD).hurtAndBreak(1, living, (entity) -> entity.broadcastBreakEvent(EquipmentSlot.HEAD));

				if (isWearingScubaOnSlot(living, EquipmentSlot.CHEST))
					living.getItemBySlot(EquipmentSlot.CHEST).hurtAndBreak(1, living, (entity) -> entity.broadcastBreakEvent(EquipmentSlot.CHEST));

				if (isWearingScubaOnSlot(living, EquipmentSlot.LEGS))
					living.getItemBySlot(EquipmentSlot.LEGS).hurtAndBreak(1, living, (entity) -> entity.broadcastBreakEvent(EquipmentSlot.LEGS));

				if (isWearingScubaOnSlot(living, EquipmentSlot.FEET))
					living.getItemBySlot(EquipmentSlot.FEET).hurtAndBreak(1, living, (entity) -> entity.broadcastBreakEvent(EquipmentSlot.FEET));

			}
		}
	}

	@SubscribeEvent
	public static void breakSpeed(PlayerEvent.BreakSpeed event)
	{
		// increase break speed to around normal speed
		if (isWearingScubaOnSlot(event.getEntity(), EquipmentSlot.CHEST) && event.getEntity().isEyeInFluidType(ForgeMod.WATER_TYPE.get()) && !event.getEntity().hasEffect(MobEffects.DIG_SLOWDOWN))
			event.setNewSpeed(event.getOriginalSpeed() + 0.7F);
	}

	public static boolean isWearingScuba(LivingEntity living)
	{
		return isWearingScubaOnSlot(living, EquipmentSlot.HEAD) || isWearingScubaOnSlot(living, EquipmentSlot.CHEST) || isWearingScubaOnSlot(living, EquipmentSlot.LEGS) || isWearingScubaOnSlot(living, EquipmentSlot.FEET);
	}

	public static boolean isWearingFullScuba(LivingEntity living)
	{
		return isWearingScubaOnSlot(living, EquipmentSlot.HEAD) && isWearingScubaOnSlot(living, EquipmentSlot.CHEST) && isWearingScubaOnSlot(living, EquipmentSlot.LEGS) && isWearingScubaOnSlot(living, EquipmentSlot.FEET);
	}

	public static boolean isWearingScubaOnSlot(LivingEntity living, EquipmentSlot pieceValue)
	{
		return SCUBA_GEAR.contains(living.getItemBySlot(pieceValue).getItem());
	}

	// easy way of checking, adding, and removing all the attribute modifiers
	private static void checkAddAndRemoveModifier(LivingEntity living, boolean condition, Attribute attribute, AttributeModifier modifier)
	{
		if (condition)
		{
			if (!living.getAttribute(attribute).hasModifier(modifier))
				living.getAttribute(attribute).addTransientModifier(modifier);
		}
		else
			removeModifier(living, attribute, modifier);
	}

	/*an easier way to remove a modifier, without having to manually check if it even has it every single time*/
	private static void removeModifier(LivingEntity living, Attribute attribute, AttributeModifier modifier)
	{
		if (living.getAttribute(attribute).hasModifier(modifier))
			living.getAttribute(attribute).removeModifier(modifier);
	}

	/*could be performance intensive, but it was requested. There's a config option to make it less intensive.*/
	private static boolean isRuinOrWreckNearby(Entity entity)
	{
		// BuiltinStructures
		if (entity.level instanceof ServerLevel world)
		{
			BlockPos pos = entity.blockPosition();

			if (world.structureManager().getStructureWithPieceAt(pos, BuiltinStructures.OCEAN_RUIN_COLD).isValid() || world.structureManager().getStructureWithPieceAt(pos, BuiltinStructures.OCEAN_RUIN_WARM).isValid())
				return true;

			int radius = ScubaConfig.COMMON.getMaxDistanceFromRuin();
			final BlockPos min = pos.offset(-radius, -5, -radius);
			final BlockPos max = pos.offset(radius, 5, radius);

			for (BlockPos posAround : BlockPos.MutableBlockPos.betweenClosedStream(min, max).map(BlockPos::immutable).collect(Collectors.toList()))
			{
				if (world.structureManager().getStructureWithPieceAt(posAround, BuiltinStructures.OCEAN_RUIN_COLD).isValid() || world.structureManager().getStructureWithPieceAt(posAround, BuiltinStructures.OCEAN_RUIN_WARM).isValid())
					return true;
			}
		}

		return false;
	}
}
